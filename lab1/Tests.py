import Test
from solve import digit_occurrences

Test.assert_equals(digit_occurrences(51, 55, 5), 6)
Test.assert_equals(digit_occurrences(1, 8, 9), 0)
Test.assert_equals(digit_occurrences(71, 77, 2), 1)
Test.assert_equals(digit_occurrences(1, 14, 4), 2)
Test.assert_equals(digit_occurrences(20, 30, 2), 11)
Test.assert_equals(digit_occurrences(18, 37, 3), 10)
Test.assert_equals(digit_occurrences(5, 335, 6), 63)
Test.assert_equals(digit_occurrences(-8, -1, 8), 1)
Test.assert_equals(digit_occurrences(-5, -5, 4), 0)
Test.assert_equals(digit_occurrences(-5, -5, 5), 1)
Test.assert_equals(digit_occurrences(-50, -45, 4), 5)
Test.assert_equals(digit_occurrences(-500, -45, 4), 190)