def digit_occurrences(start, end, val):
    ans = 0
    
    for i in range(start, end + 1):
        n = abs(i)

        while (n > 0):
            if (n % 10 == val):
                ans += 1

            n = n // 10

    return ans