from Lab3 import perimeter

def Test(a, b):
    print("True") if a == b else print("False")
    
Test(perimeter([2,3,4,None],[2,3,4,5],len([2,3,4,None])),5.657)
Test(perimeter([-4,0,6,4,None],[2,5,3,-2,1],len([-4,0,6,4,None])),25.654)
Test(perimeter([-4,0,6,None,4],[2,5,3,-2,1],len([-4,0,6,None,4])),21.374)
Test(perimeter([None,4,0,6,4],[2,5,3,-2,1],len([None,4,0,6,4])),0)
Test(perimeter([0,0,1,1,None],[0,1,1,0,1],len([0,0,1,1,None])),4)