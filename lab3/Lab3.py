import math

def perimeter(x,y,n):
    per = 0.0
    for i in range(0,n-1):
        if(x[i+1]!=None and x[i]!=None):
            per = per + math.sqrt((x[i+1]-x[i])**2 + (y[i+1]-y[i])**2)
            m = i+1
        else: break
    if(x[0]!= None):
        per += math.sqrt((x[m]-x[0])**2 + (y[m]-y[0])**2)
    return round(per,3)

if __name__ == "__main__": 
    x1= [-4,0,6,4,None]
    y1= [2,5,3,-2,1]
    print(perimeter(x1,y1,len(x1)))
        
    

    