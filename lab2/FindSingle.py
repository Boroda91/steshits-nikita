def FindSingle(arr, n):
    a = arr[0]
    for i in range(1,n):
        a = a ^ arr[i]
    return a

arr1 = [4,1,2,1,2]
arr2 = [2,2,1]
arr3=[-1,1,1,2,2]
arr4=[1111111,222222,222222,1111111,1000000]
print("Single numeral is: ", FindSingle(arr1,len(arr1)))
print("Single numeral is: ", FindSingle(arr2,len(arr2)))
print("Single numeral is: ", FindSingle(arr3,len(arr3)))
print("Single numeral is: ", FindSingle(arr4,len(arr4)))
